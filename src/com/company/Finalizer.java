package com.company;

public class Finalizer {
    private final Object object;

    public Finalizer(Object object) {
        this.object = object;
    }

    @Override
    protected void finalize() {
        System.out.println("Finalizer с " + Object.class.getSimpleName() + " был удален");
    }
}
