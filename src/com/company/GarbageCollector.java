package com.company;

public class GarbageCollector {

    public static void main(String[] args) {
        Object a = getFinalize(new Integer(100));
        Object b = getFinalize(new Long(100));
        Object c = getFinalize(new String("100"));
        a = null;
        a = c;
        c = b;
        b = a; // Line7
        System.gc();
    }

    private static Finalizer getFinalize(Object object) {
        return new Finalizer(object);
    }
}
